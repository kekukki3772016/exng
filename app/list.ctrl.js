(function () {
    'use strict';

    var app = angular.module('app');

    app.controller('ListCtrl', Ctrl);

    function Ctrl($http) {

        var vm = this;

        vm.deletePost = deletePost;

        init();

        function init(){
            $http.get('http://localhost:3000/posts').then(function(result){
                vm.posts = result.data;
                console.log(JSON.stringify(result));
            });
        }

        function deletePost(postId){
            $http.delete('http://localhost:3000/posts/' + postId).then(function(result){
                init();
            });
        }

    }

})();
