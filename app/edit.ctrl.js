(function () {
    'use strict';

    angular.module('app').controller('EditCtrl', Ctrl);

    function Ctrl($http, $location, $routeParams, readonly) {

        var vm = this;
        vm.posts = [];
        vm.title = '';
        vm.text = '';
        vm.readonly = readonly;
        vm.addNew = addNew;

        init();

        function init(){
            if($routeParams.id == undefined) {
                return;
            }

            $http.get('http://localhost:3000/posts/' + $routeParams.id).then(function(result){
                vm.title = result.data.title;
                vm.text = result.data.text;
            });
        }

        function addNew(){
            var newPost = {
                title : vm.title,
                text : vm.text
            }
            $http.post('http://localhost:3000/posts', newPost).then(function(){
                $location.path('/list');
            });
        }

    }

})();
