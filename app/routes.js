(function () {
    'use strict';

    angular.module('app').config(conf);

    function conf($routeProvider){

        $routeProvider.when('/list', {
            templateUrl : 'app/list.html',
            controller : 'ListCtrl',
            controllerAs : 'vm'
        }).when('/new', {
            templateUrl : 'app/edit.html',
            controller : 'EditCtrl',
            controllerAs : 'vm',
            resolve : {
                readonly : function(){
                    return false;
                }
            }
        }).when('/view/:id', {
            templateUrl : 'app/edit.html',
            controller : 'EditCtrl',
            controllerAs : 'vm',
            resolve : {
                readonly : function(){
                    //return $http.get(... identifikaatorid
                    return true;
                }
            }
        }).otherwise('/list');

    }

})();